# Sistema de gestão de senhas #

Prova teste para vaga de Analista de Sistemas III

## Pré-requisitos do projeto ##

1. NetBeans 8.0.2;
1. TomCat 8.0.26;
1. Windows 8.

## Passos para execução do projeto ##

1. Abra o NetBeans em modo administrador;
1. Baixe o código fonte condido em https://bitbucket.org/turatti23/paripassu_sistema_gestao_senhas/ (recomendo o uso do sourcetree para essa operação);
1. Abra o projeto no NetBeans. Clique com o botão direito do mouse em "GestoDeSenhas" -> "Pacotes de Teste" -> "br.com.paripassu.gestaodesenhas" -> "requisitos_funcionais_test.java". No menu que segue, escolha "Testar Arquivo";
1. Clique com o botão direito do mouse na raiz do projeto ("GestaoDeSenhas"). No menu que segue, escolha "Executar";
1. Caso lhe seja solicitado, escolha o Apache Tomcat como "servidor de implantação".

## Como utilizar o sistema ##

1. Clique em Senhas -> Normal, para gerar uma senha do tipo Normal;
1. Clique em Senhas -> Preferencial, para gerar uma senha do tipo Preferencial;
1. Clique em Gerenciamento -> Próxima senha para que o sistema chame a próxima senha da fila de senhas;
1. Clique em Gerenciamento -> Reiniciar contador de senhas para que o sistema zere as filas de senha e retorne o sistema ao seu estado inicial;
1. No painel verde superior, sempre é apresentada a senha que está sendo chamada atualmente;
1. Já o painel inferior, mostra a última senha gerada através do menu de Senhas.