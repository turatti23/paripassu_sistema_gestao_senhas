package br.com.paripassu.gestaodesenhas;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Marcos
 */
public class requisitos_funcionais_test {
    
    public requisitos_funcionais_test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void DeveSerPossivelGerarNovaSenhaNormal() {
        Senha senha = new SenhaNormal(1);
        assertTrue("O tipo da senha gerada é: " + senha.getTipo()+ " e o valor esperado é: N",
                senha.getTipo() == 'N');
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: N0001",
                senha.getValor().equals("N0001"));
        senha = new SenhaNormal(10);
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: N0010",
                senha.getValor().equals("N0010"));
        senha = new SenhaNormal(100);
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: N0100",
                senha.getValor().equals("N0100"));
        senha = new SenhaNormal(1000);
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: N1000",
                senha.getValor().equals("N1000"));
    }
    
    @Test
    public void DeveSerPossivelGerarNovaSenhaPreferencial(){
        Senha senha = new SenhaPreferencial(1);
        assertTrue("O tipo da senha gerada é: " + senha.getTipo()+ " e o valor esperado é: P",
                senha.getTipo() == 'P');
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: P0001",
                senha.getValor().equals("P0001"));
        senha = new SenhaPreferencial(10);
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: P0010",
                senha.getValor().equals("P0010"));
        senha = new SenhaPreferencial(100);
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: P0100",
                senha.getValor().equals("P0100"));
        senha = new SenhaPreferencial(1000);
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: P1000",
                senha.getValor().equals("P1000"));
        
    }
    
    @Test
    public void DeveSerPossivelCriarVariasSenhas(){
        GeradorDeSenhas geradorDeSenhas = new GeradorDeSenhas();
        
        Senha senha = geradorDeSenhas.gerarNovaSenhaNormal();
        assertTrue("O tipo da senha gerada é: " + senha.getTipo()+ " e o valor esperado é: N",
                senha.getTipo() == 'N');
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: N0001",
                senha.getValor().equals("N0001"));
        
        senha = geradorDeSenhas.gerarNovaSenhaNormal();
        assertTrue("O tipo da senha gerada é: " + senha.getTipo()+ " e o valor esperado é: N",
                senha.getTipo() == 'N');
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: N0002",
                senha.getValor().equals("N0002"));
        
        senha = geradorDeSenhas.gerarNovaSenhaPreferencial();
        assertTrue("O tipo da senha gerada é: " + senha.getTipo()+ " e o valor esperado é: P",
                senha.getTipo() == 'P');
        assertTrue("O valor da senha gerada é: " + senha.getValor() + " e o valor esperado é: P0001",
                senha.getValor().equals("P0001"));
    }
    
    private static final int SEQUENCIA_1 = 1;
    private static final int LIMITE_EXCEDIDO = 10000;
    @Test
    public void SeGeradorDeSenhasChegarAoLimiteDeveRetornarAoInicio(){
        GeradorDeSenhas geradorDeSenhas = new GeradorDeSenhas();
        
        Senha senhaAutoGerada, senhaAuxiliar;
        for (int i = SEQUENCIA_1; i <= LIMITE_EXCEDIDO; i++) {
            senhaAutoGerada = geradorDeSenhas.gerarNovaSenhaNormal();
            senhaAuxiliar = (i != LIMITE_EXCEDIDO) ? new SenhaNormal(i) : new SenhaNormal(SEQUENCIA_1);
            
            assertTrue("senhaAutoGerada:" + senhaAutoGerada.getValor() + " e diferente da senhaAuxiliar: " + senhaAuxiliar.getValor(),
                    senhaAutoGerada.getValor().equals(senhaAuxiliar.getValor()));
        }
        
        for (int i = SEQUENCIA_1; i <= LIMITE_EXCEDIDO; i++) {
            senhaAutoGerada = geradorDeSenhas.gerarNovaSenhaPreferencial();
            senhaAuxiliar = (i != LIMITE_EXCEDIDO) ? new SenhaPreferencial(i) : new SenhaPreferencial(SEQUENCIA_1);
            
            assertTrue("senhaAutoGerada:" + senhaAutoGerada.getValor() + " e diferente da senhaAuxiliar: " + senhaAuxiliar.getValor(),
                    senhaAutoGerada.getValor().equals(senhaAuxiliar.getValor()));
        }
    }
    
    @Test
    public void DeveSerPossivelChamarAProximaSenhaNormal(){
        GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
        Pessoa gerente = new Gerente();
        
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaNormal();
        
        assertTrue(gerenciadorDeSenhas.chamarProximaSenha(gerente).equals("N0001"));
        assertTrue(gerenciadorDeSenhas.chamarProximaSenha(gerente).equals("N0002"));
        assertTrue(gerenciadorDeSenhas.chamarProximaSenha(gerente).equals("N0003"));
    }
    
    @Test
    public void DeveSerPossivelChamarTodasAsSenhasNormais(){
        GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
        Pessoa gerente = new Gerente();
        
        String valorSenha;
        int numeroSenhaAux = 0;
        String valorSenhaAux;
        
        for (int i=0; i<=10000; i++){
            gerenciadorDeSenhas.gerarSenhaNormal();
        }
        
        for (int i=1; i<=10000; i++){
            numeroSenhaAux++;
            if (numeroSenhaAux > 9999)
                numeroSenhaAux = 1;
                
            if (numeroSenhaAux>0 && numeroSenhaAux<10)
                valorSenhaAux = "N000";
            else if (numeroSenhaAux>=10 && numeroSenhaAux<100)
                valorSenhaAux = "N00";
            else if (numeroSenhaAux>=100 & numeroSenhaAux<1000)
                valorSenhaAux = "N0";
            else 
                valorSenhaAux = "N";
            valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
            assertTrue("atual:" + valorSenha + "Esperado: " + valorSenhaAux + i,valorSenha.equals(valorSenhaAux + numeroSenhaAux));
        }
    }
    
    @Test
    public void DeveSerPossivelChamarAProximaSenhaPreferencial(){
        GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
        Pessoa gerente = new Gerente();
        
        gerenciadorDeSenhas.gerarSenhaPreferencial();
        gerenciadorDeSenhas.gerarSenhaPreferencial();
        gerenciadorDeSenhas.gerarSenhaPreferencial();
        
        assertTrue(gerenciadorDeSenhas.chamarProximaSenha(gerente).equals("P0001"));
        assertTrue(gerenciadorDeSenhas.chamarProximaSenha(gerente).equals("P0002"));
        assertTrue(gerenciadorDeSenhas.chamarProximaSenha(gerente).equals("P0003"));
    }
    
    @Test
    public void DeveSerPossivelChamarTodasAsSenhasPreferenciais(){
        GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
        Pessoa gerente = new Gerente();
        
        String valorSenha;
        int numeroSenhaAux = 0;
        String valorSenhaAux;
        
        for (int i=0; i<=10000; i++){
            gerenciadorDeSenhas.gerarSenhaPreferencial();
        }
        
        for (int i=1; i<=10000; i++){
            numeroSenhaAux++;
            if (numeroSenhaAux > 9999)
                numeroSenhaAux = 1;
                
            if (numeroSenhaAux>0 && numeroSenhaAux<10)
                valorSenhaAux = "P000";
            else if (numeroSenhaAux>=10 && numeroSenhaAux<100)
                valorSenhaAux = "P00";
            else if (numeroSenhaAux>=100 & numeroSenhaAux<1000)
                valorSenhaAux = "P0";
            else 
                valorSenhaAux = "P";
            valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
            assertTrue("atual:" + valorSenha + "Esperado: " + valorSenhaAux + i,valorSenha.equals(valorSenhaAux + numeroSenhaAux));
        }
    }
    
    @Test
    public void OSistemaDeveDarPrioridadeAsSenhasPreferenciais(){
        GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
        Pessoa gerente = new Gerente();
        
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaPreferencial();
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaPreferencial();
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaPreferencial();
        
        String valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: P0001", valorSenha.equals("P0001"));
        valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: P0002", valorSenha.equals("P0002"));
        valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: P0003", valorSenha.equals("P0003"));
        valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: N0001", valorSenha.equals("N0001"));
        valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: N0002", valorSenha.equals("N0002"));
        valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: N0003", valorSenha.equals("N0003"));
        valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: N0004", valorSenha.equals("N0004"));
    }
    
    @Test 
    public void SomenteOGerenteSeraCaspazDeReiniciarAContagem(){
        Pessoa cliente = new Cliente();
        Pessoa gerente = new Gerente();
        GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
        
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaPreferencial();
        
        gerenciadorDeSenhas.reiniciarContagem(gerente);
        
        assertTrue(gerenciadorDeSenhas.chamarProximaSenha(gerente).equals("Não há próximas senhas."));
        
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaPreferencial();

        gerenciadorDeSenhas.reiniciarContagem(cliente);
        
        String valorProximaSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertFalse(valorProximaSenha.equals("Não há próximas senhas."));
        assertTrue(valorProximaSenha.equals("P0001"));
        valorProximaSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue(valorProximaSenha.equals("N0001"));

        gerenciadorDeSenhas.reiniciarContagem(gerente);

        assertTrue(gerenciadorDeSenhas.acompanharUltimaSenhaChamada().equals("Nenhuma senha foi chamada!"));
    }
    
    @Test
    public void SomenteOGerenteSeraCapazDeChamarAProximaSenha(){
        GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
        Pessoa gerente = new Gerente();
        Pessoa cliente = new Cliente();
        
        gerenciadorDeSenhas.gerarSenhaNormal();
        gerenciadorDeSenhas.gerarSenhaPreferencial();
        
        String valorSenha = gerenciadorDeSenhas.chamarProximaSenha(cliente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: Não possui acesso para está ação", valorSenha.equals("Não possui acesso para está ação"));
        valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: P0001", valorSenha.equals("P0001"));
        valorSenha = gerenciadorDeSenhas.chamarProximaSenha(gerente);
        assertTrue("valor atual: " + valorSenha + "valor esperado: N0001", valorSenha.equals("N0001"));
    }
    
    @Test
    public void DeveSerPosivelAcompanharAChamadaDasSenhas(){
        GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
        Pessoa gerente = new Gerente();
        
        gerenciadorDeSenhas.gerarSenhaNormal();
        
        String ultimaSenhaChamada = gerenciadorDeSenhas.acompanharUltimaSenhaChamada();
        assertTrue(ultimaSenhaChamada.equals("Nenhuma senha foi chamada!"));
        
        gerenciadorDeSenhas.chamarProximaSenha(gerente);
        
        ultimaSenhaChamada = gerenciadorDeSenhas.acompanharUltimaSenhaChamada();
        assertTrue(ultimaSenhaChamada.equals("N0001"));
    }
}
