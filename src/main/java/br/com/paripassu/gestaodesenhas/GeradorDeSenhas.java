/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.paripassu.gestaodesenhas;

/**
 *
 * @author Marcos
 */
class GeradorDeSenhas {
    private int ultimaSenhaNormal = 0;
    private int ultimaSenhaPreferencial = 0;
    

    Senha gerarNovaSenhaNormal() {
        this.ultimaSenhaNormal++;
        if (ultimaSenhaNormal == 10000)
            this.ultimaSenhaNormal = 1;
        return new SenhaNormal(ultimaSenhaNormal);
    }

    Senha gerarNovaSenhaPreferencial() {
        this.ultimaSenhaPreferencial++;
        if (ultimaSenhaPreferencial == 10000)
            this.ultimaSenhaPreferencial = 1;
        return new SenhaPreferencial(ultimaSenhaPreferencial);
    }

    void reiniciarContagem(Pessoa pessoa) {
        if (pessoa.getTipo() == 'G') {
            this.ultimaSenhaNormal=0;
            this.ultimaSenhaPreferencial=0;
        }
    }
}
