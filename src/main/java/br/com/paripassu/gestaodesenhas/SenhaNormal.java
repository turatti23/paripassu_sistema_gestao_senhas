/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.paripassu.gestaodesenhas;

/**
 *
 * @author Marcos
 */
class SenhaNormal implements Senha {
    private final int numeroDaSenha;

    SenhaNormal(int numeroSenha) {
        this.numeroDaSenha = numeroSenha;
    }

    @Override
    public char getTipo() {
        return 'N';
    }

    @Override
    public String getValor() {
        if (numeroDaSenha < 10){
            return getTipo() + "000" + numeroDaSenha;
        } else if (numeroDaSenha < 100){
            return getTipo() + "00" + numeroDaSenha;
        } else if (numeroDaSenha <1000){
            return getTipo() + "0" + numeroDaSenha;
        } else {
            return getTipo() + String.valueOf(numeroDaSenha);
        }
    }
}
