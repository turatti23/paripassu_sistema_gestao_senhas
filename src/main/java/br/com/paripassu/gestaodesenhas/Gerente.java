/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.paripassu.gestaodesenhas;

import br.com.paripassu.gestaodesenhas.Pessoa;

/**
 *
 * @author Marcos
 */
class Gerente implements Pessoa {

    public Gerente() {
    }

    @Override
    public char getTipo() {
        return 'G';
    }
    
}
