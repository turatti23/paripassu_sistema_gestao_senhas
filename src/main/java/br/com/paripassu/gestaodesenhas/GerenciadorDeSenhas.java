/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.paripassu.gestaodesenhas;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marcos
 */
class GerenciadorDeSenhas {
    private Senha ultimaSenhaChamada = null;
    private List<Senha> listaDeSenhasNormais = null;
    private List<Senha> listaDeSenhasPreferencias = null;
    private GeradorDeSenhas geradorDeSenhas = null;
    
    public GerenciadorDeSenhas() {
        listaDeSenhasNormais = new ArrayList<>();
        listaDeSenhasPreferencias = new ArrayList<>();
        geradorDeSenhas = new GeradorDeSenhas();
    }
    
    Senha gerarSenhaNormal() {
        Senha senhaNormal = geradorDeSenhas.gerarNovaSenhaNormal();
        
        enfileraSenha(senhaNormal);
        
        return senhaNormal;
    }
    
    Senha gerarSenhaPreferencial() {
        Senha senhaPreferencial = geradorDeSenhas.gerarNovaSenhaPreferencial();
        
        enfileraSenha(senhaPreferencial);
        
        return senhaPreferencial;
    }
    
    private void enfileraSenha(Senha novaSenha) {
        if (novaSenha.getTipo() == 'P')
            listaDeSenhasPreferencias.add(novaSenha);
        else
            listaDeSenhasNormais.add(novaSenha);
    }

    String chamarProximaSenha(Pessoa tipoPessoa) {
        if (tipoPessoa.getTipo() == 'G'){
            Senha proximaSenha = null;

            if (listaDeSenhasPreferencias.size() > 0) {
                proximaSenha = listaDeSenhasPreferencias.get(0);
                listaDeSenhasPreferencias.remove(0);
            }
            else if (listaDeSenhasNormais.size() > 0) {
                proximaSenha = listaDeSenhasNormais.get(0);
                listaDeSenhasNormais.remove(0);
            }
            ultimaSenhaChamada = proximaSenha;
            
            return (proximaSenha != null) ?
                    proximaSenha.getValor() :
                    "Não há próximas senhas.";
        } else
            return "Não possui acesso para está ação";
    }

    void reiniciarContagem(Pessoa pessoa) {
        if (pessoa.getTipo() == 'G'){
            listaDeSenhasNormais.clear();
            listaDeSenhasPreferencias.clear();
            ultimaSenhaChamada = null;
            geradorDeSenhas.reiniciarContagem(pessoa);
        }
    }

    String acompanharUltimaSenhaChamada() {
        return (ultimaSenhaChamada != null) ?
                ultimaSenhaChamada.getValor() :
                "Nenhuma senha foi chamada!";
    }
}
