/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.paripassu.gestaodesenhas;

import com.google.gson.Gson;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Marcos
 */
@Path("/")
public class ServicosWeb {
    private static final GerenciadorDeSenhas gerenciadorDeSenhas = new GerenciadorDeSenhas();
    private Gson gson = new Gson();
    
    @GET
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON)
    public Response ping() {
        String response = "Servidor ativo.";
        return Response.status(200).entity(gson.toJson(response)).build();
    }
    
    @GET
    @Path("/acompanharUltimaSenhaChamada")
    @Produces(MediaType.APPLICATION_JSON)
    public Response acompanharUltimaSenhaChamada() {
        return Response.status(200).entity(gson.toJson(gerenciadorDeSenhas.acompanharUltimaSenhaChamada())).build();
    }
    
    @GET
    @Path("/gerarSenhaNormal")
    @Produces(MediaType.APPLICATION_JSON)
    public Response gerarSenhaNormal() {
        Senha senhaNormal = gerenciadorDeSenhas.gerarSenhaNormal();
        return Response.status(200).entity(gson.toJson(senhaNormal.getValor())).build();
    }
    
    @GET
    @Path("/gerarSenhaPreferencial")
    @Produces(MediaType.APPLICATION_JSON)
    public Response gerarSenhaPreferencial() {
        Senha senhaPreferencial = gerenciadorDeSenhas.gerarSenhaPreferencial();
        return Response.status(200).entity(gson.toJson(senhaPreferencial.getValor())).build();
    }
    
    @GET
    @Path("/proximaSenhaParaAtendimento")
    @Produces(MediaType.APPLICATION_JSON)
    public Response proximaSenha() {
        Pessoa pessoa = new Gerente();
        return Response.status(200).entity(gson.toJson(gerenciadorDeSenhas.chamarProximaSenha(pessoa))).build();
    }
    
    @GET
    @Path("/zerarSenhas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response zerarSenhas() {
        Pessoa pessoa = new Gerente();
        gerenciadorDeSenhas.reiniciarContagem(pessoa);
        return Response.status(200).entity(gson.toJson("Reiniciada a contagem")).build();
    }
    
    
}
