<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>PariPassu - Gerador de Senhas</title>
    <style>
        .container-fluid {
            background-color: #FFFFFF;
            border-color: #449d44;
        }
    </style>
</head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="js/GestaoDeSenhas.js"></script>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                    <a class="navbar-brand" href="http://www.paripassu.com.br/" target="_blank">
                        <img style="height: 30px" src="images/paripassu_logo.jpg">
                    </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#" style="background-color: #449d44">. <span class="sr-only"></span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Senhas <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="GerarSenhaNormal">Normal</a></li>
                            <li><a href="#" id="GerarSenhaPreferencial">Preferencial</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gerenciamento <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="ChamarProximaSenha">Próxima senha</a></li>
                            <li><a href="#" id="ReiniciarContagemDeSenhas">Reiniciar contador de senhas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sobre <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                                <li><a href="https://www.linkedin.com/in/mturatti" target="_blank">Powered by Marcos Turatti</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div style="padding-bottom:25px; padding-top: 50px; padding-left: 100px; padding-right: 100px">
        <div class="alert alert-success" role="alert" style="padding:40px; text-align: center"><span style="font-size: 40px" id="UltimaSenhaChamada">Aguardo chamada da próxima senha...</span></div>
    </div>
    <div style="padding-top:25px; padding-bottom: 50px; padding-left: 100px; padding-right: 100px">
        <div class="alert alert-success" role="alert" style="padding:40px; text-align: center"><span style="font-size: 40px" id="UltimaSenhaGerada">Aguardo geração da próxima senha...</span></div>
    </div>
    
    <div style="text-align: center; vertical-align: middle;">
        <img src="images/paripassu_logo.jpg">
    </div>
</body>

<script type="text/javascript">
$(document).ready(function(){
    MostrarUltimaSenhaChamada();
    $("#UltimaSenhaGerada").html("Aguardo geração da próxima senha...");
    
    $("#GerarSenhaNormal").click(function(){
        $("#UltimaSenhaGerada").html("A última senha gerada foi: <b>" + restCallGetJson("gerarSenhaNormal") + "</b>");
    });
    $("#GerarSenhaPreferencial").click(function(){
        $("#UltimaSenhaGerada").html("A última senha gerada foi: <b>" + restCallGetJson("gerarSenhaPreferencial") + "</b>");
    });
    $("#ChamarProximaSenha").click(function(){
        restCallGetJson("proximaSenhaParaAtendimento");
        MostrarUltimaSenhaChamada();
    });
    $("#ReiniciarContagemDeSenhas").click(function(){
        restCallGetJson("zerarSenhas");
        MostrarUltimaSenhaChamada();
        $("#UltimaSenhaGerada").html("Aguardo geração da próxima senha...");
    });
    
    function MostrarUltimaSenhaChamada() {
        $("#UltimaSenhaChamada").html("Nenhuma senha foi chamada!");

        var ultimaSenhaChamada = restCallGetJson("acompanharUltimaSenhaChamada");
        if (ultimaSenhaChamada !== "Nenhuma senha foi chamada!")
            $("#UltimaSenhaChamada").html("Atenção! Senha: <b>" + restCallGetJson("acompanharUltimaSenhaChamada") + "</b> favor dirigir-se ao atendimento.");
    }
});
</script>
</html>